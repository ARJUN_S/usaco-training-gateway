/*
ID: arjun.s4
LANG: JAVA
TASK: castle
*/

import java.util.*;
import java.io.*;

public class castle
{
    public static void main(String[] args)
    {
        Scanner s = IO.getScan("castle.in");
        PrintWriter pw = IO.getPW("castle.out");

        int M = s.nextInt();
        int N = s.nextInt();
        Square[][] squares = new Square[N][M];
        for (int n = 0; n < N; n++)
        {
            for (int m = 0; m < M; m++)
                squares[n][m] = interpretModule(m, n, s.nextInt());
        }

        HashMap<Square, ArrayList<Square>> layout = getGraph(M, N, squares, -1, -1, -1, -1);

        ArrayList<ArrayList<Square>> cc = computeCC(layout);
        pw.println(cc.size());
        int maxSize = 0;
        for (ArrayList<Square> c : cc)
            maxSize = Math.max(maxSize, c.size());
        pw.println(maxSize);

        maxSize = 0;
        int[] optimWall = new int[3];
        for (int n = 0; n < N; n++)
        {
            for (int m = 0; m < M; m++)
            {
                if (m - 1 >= 0 && squares[n][m - 1].getWalls()[2] != -1)
                {
                    layout = getGraph(M, N, squares, m, m - 1, n, n);
                    maxSize = decide(layout, maxSize, optimWall, m - 1, n, 2);
                }
                if (m + 1 <= M - 1 && squares[n][m + 1].getWalls()[0] != -1)
                {
                    layout = getGraph(M, N, squares, m, m + 1, n, n);
                    maxSize = decide(layout, maxSize, optimWall, m, n, 2);
                }
                if (n - 1 >= 0 && squares[n - 1][m].getWalls()[3] != -1)
                {
                    layout = getGraph(M, N, squares, m, m, n, n - 1);
                    maxSize = decide(layout, maxSize, optimWall, m, n, 1);
                }
                if (n + 1 <= N - 1 && squares[n + 1][m].getWalls()[1] != -1)
                {
                    layout = getGraph(M, N, squares, m, m, n, n + 1);
                    maxSize = decide(layout, maxSize, optimWall, m, n + 1, 1);
                }
            }
        }
        pw.println(maxSize);
        pw.println((optimWall[1] + 1) + " " + (optimWall[0] + 1) + " " + (optimWall[2] == 2 ? "E" : "N"));
        pw.close();
    }

    public static int decide(HashMap<Square, ArrayList<Square>> layout, int maxSize, int[] optimWall, int m, int n, int dir)
    {
        ArrayList<ArrayList<Square>> cc = computeCC(layout);
        for (ArrayList<Square> c : cc)
        {
            // System.out.println(m + ", " + n + ", " + c);
            if (maxSize < c.size())
            {
                maxSize = c.size();
                optimWall[0] = m;
                optimWall[1] = n;
                optimWall[2] = dir;
            }
            else if (maxSize == c.size())
            {
                maxSize = c.size();
                if (m < optimWall[0])
                {
                    optimWall[0] = m;
                    optimWall[1] = n;
                    optimWall[2] = dir;
                }
                else if (m == optimWall[0])
                {
                    if (n > optimWall[1])
                    {
                        optimWall[0] = m;
                        optimWall[1] = n;
                        optimWall[2] = dir;
                    }
                    else if (n == optimWall[1])
                    {
                        optimWall[0] = m;
                        optimWall[1] = n;
                        optimWall[2] = 1;
                    }
                }
            }
        }
        return maxSize;
    }

    public static HashMap<Square, ArrayList<Square>> getGraph(int M, int N, Square[][] squares, int m1, int m2, int n1, int n2)
    {
        HashMap<Square, ArrayList<Square>> layout = new HashMap<Square, ArrayList<Square>>();
        for (int n = 0; n < N; n++)
        {
            for (int m = 0; m < M; m++)
            {
                layout.put(squares[n][m], new ArrayList<Square>());
                if ((m1 == m && m2 == m - 1 && n1 == n && n2 == n) || (m != 0 && squares[n][m - 1].getWalls()[2] == -1))
                    layout.get(squares[n][m]).add(squares[n][m - 1]);
                if ((m1 == m && m2 == m && n1 == n && n2 == n - 1) || (n != 0 && squares[n - 1][m].getWalls()[3] == -1))
                    layout.get(squares[n][m]).add(squares[n - 1][m]);
                if ((m1 == m && m2 == m + 1 && n1 == n && n2 == n) || (m != M - 1 && squares[n][m + 1].getWalls()[0] == -1))
                    layout.get(squares[n][m]).add(squares[n][m + 1]);
                if ((m1 == m && m2 == m && n1 == n && n2 == n + 1) || (n != N - 1 && squares[n + 1][m].getWalls()[1] == -1))
                    layout.get(squares[n][m]).add(squares[n + 1][m]);
            }
        }
        return layout;
    }

    public static ArrayList<ArrayList<Square>> computeCC(HashMap<Square, ArrayList<Square>> g)
    {
        ArrayList<ArrayList<Square>> res = new ArrayList<ArrayList<Square>>();

        ArrayList<Square> nodes = new ArrayList<Square>();
        nodes.addAll(g.keySet());
        boolean[] vis = new boolean[nodes.size()];
        for (int i = 0; i < nodes.size(); i++)
        {
            if (!vis[i])
                res.add(BFS(g, i, nodes, vis));
        }
        return res;
    }

    public static ArrayList<Square> BFS(HashMap<Square, ArrayList<Square>> g, int i, ArrayList<Square> nodes, boolean[] vis)
    {
        ArrayList<Square> res = new ArrayList<Square>();

        vis[i] = true;
        res.add(nodes.get(i));
        ArrayList<Integer> queue = new ArrayList<Integer>();
        queue.add(i);
        while (queue.size() != 0)
        {
            Square v = nodes.get(queue.remove(0));
            for (Square w : g.get(v))
            {
                int idx = nodes.indexOf(w);
                if (!vis[idx])
                {
                    vis[idx] = true;
                    res.add(w);
                    queue.add(idx);
                }
            }
        }
        return res;
    }

    public static Square interpretModule(int m, int n, int num)
    {  
        int[] walls = {-1, -1, -1, -1};
        if (num >= 8)
        {
            walls[3] = 8;
            num -= 8;
        }
        if (num >= 4)
        {
            walls[2] = 4;
            num -= 4;
        }
        if (num >= 2)
        {
            walls[1] = 2;
            num -= 2;
        }
        if (num == 1)
            walls[0] = 1;
        return new Square(m, n, walls);
    }
}

class Square
{
    private int m;
    private int n;
    private int[] walls;

    public Square(int m, int n, int[] walls)
    {
        this.m = m;
        this.n = n;
        this.walls = walls;
    }

    public int getM()
    {
        return this.m;
    }

    public int getN()
    {
        return this.n;
    }

    public int[] getWalls()
    {
        return this.walls;
    }

    public String toString()
    {
        return "(" + (m + 1) + ", " + (n + 1) + ")";
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}