/*
ID: arjun.s4
LANG: JAVA
TASK: sprime
*/

import java.util.*;
import java.io.*;

public class sprime
{
    public static void main(String[] args)
    {
        Scanner s = IO.getScan("sprime.in");
        PrintWriter pw = IO.getPW("sprime.out");

        int N = s.nextInt();
        ArrayList<Integer> all = new ArrayList<Integer>();
        sprime(N, 0, all, 0);
        Collections.sort(all);
        for (Integer i : all)
            pw.println(i);
        pw.close();
    }

    public static void sprime(int N, int curr, ArrayList<Integer> all, int total)
    {
        if (curr == N)
            all.add(total);
        else
        {
            if (curr == 0)
            {
                for (int i = 2; i <= 7; i += 2)
                { 
                    int res = 10 * total + i;
                    if (isPrime(res))
                        sprime(N, curr + 1, all, res);
                    if (i == 2)
                        i--;
                }
            }
            else
            {
                for (int i = 1; i <= 9; i += 2)
                { 
                    int res = 10 * total + i;
                    if (isPrime(res))
                        sprime(N, curr + 1, all, res);
                }
            }
        }
    }

    public static boolean isPrime(int i)
    {
        for (int d = 3; d <= Math.sqrt(i); d += 2)
        {
            if (i % d == 0)
                return false;
        }
        return true;
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}