/*
ID: arjun.s4
LANG: JAVA
TASK: hamming
*/

import java.util.*;
import java.io.*;

public class hamming
{
    public static void main(String[] args)
    {
        Scanner s = IO.getScan("hamming.in");
        PrintWriter p = IO.getPW("hamming.out");

        int N = s.nextInt();
        int B = s.nextInt();
        int D = s.nextInt();

        int max = (int)Math.pow(2, B + 1);
        ArrayList<Integer> codewords = new ArrayList<Integer>();
        ArrayList<String> binaries = new ArrayList<String>();
        for (int d = 0; d < max; d++)
        {
            String b = binary(d);
            b = String.format("%0" + (B + 1) + "d", Integer.parseInt(b));
            boolean accept = true;
            for (String bin : binaries)
            {
                if (differ(b, bin) < D)
                    accept = false;
            }
            if (accept)
            {
                codewords.add(d);
                binaries.add(b);
            }
        }
        Collections.sort(codewords);
        for (int n = 1; n <= N; n++)
        {
            if (n % 10 == 0 || n == N)
                p.println(codewords.get(n - 1));
            else
                p.print(codewords.get(n - 1) + " ");
        }
        p.close();
    }

    private static String binary(int number)
    {
        if (number <= 1)
            return number + "";
        int remainder = number % 2; 
        return binary(number >> 1) + remainder;
    }

    private static int differ(String s1, String s2)
    {
        int numDiff = 0;
        for (int i = 0; i < s1.length(); i++)
        {
            if (s1.charAt(i) != s2.charAt(i))
                numDiff++;
        }
        return numDiff;
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}