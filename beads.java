/*
ID: arjun.s4
LANG: JAVA
TASK: beads
*/

import java.util.*;
import java.io.*;

public class beads
{
    public static void main(String[] args)
    {
        Scanner s = IO.getScan("beads.in");
        PrintWriter p = IO.getPW("beads.out");

        int N = Integer.parseInt(s.nextLine());
        String necklace = s.nextLine();
        necklace = necklace + necklace;
        int best = 0;
        int bestInd = 0;
        for (int i = 0; i < necklace.length() - 1; i++)
        {
            char notTarget = 'x';
            if (necklace.charAt(i) == 'r')
                notTarget = 'b';
            else if (necklace.charAt(i) == 'b')
                notTarget = 'r';
            int total = 0;
            for (int left = i; left >= 0; left--)
            {
                if (notTarget == 'x' && necklace.charAt(left) != 'w')
                {
                    if (necklace.charAt(left) == 'r')
                        notTarget = 'b';
                    else
                        notTarget = 'r';
                }
                else if (necklace.charAt(left) == notTarget)
                    break;
                total++;
            }
            notTarget = 'x';
            if (necklace.charAt(i + 1) == 'r')
                notTarget = 'b';
            else if (necklace.charAt(i + 1) == 'b')
                notTarget = 'r';
            for (int right = i + 1; right < necklace.length(); right++)
            {
                if (notTarget == 'x' && necklace.charAt(right) != 'w')
                {
                    if (necklace.charAt(right) == 'r')
                        notTarget = 'b';
                    else
                        notTarget = 'r';
                }
                else if (necklace.charAt(right) == notTarget)
                    break;
                total++;
            }
            if (total > best)
            {
                best = total;
                bestInd = i;
            }
        }
        if (best > N)
            best = N;
        
        p.println(best);
        p.close();
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}