/*
ID: arjun.s4
LANG: JAVA
TASK: dualpal
*/

import java.util.*;
import java.io.*;

public class dualpal
{
    public static void main(String[] args)
    {
        Scanner s = IO.getScan("dualpal.in");
        PrintWriter p = IO.getPW("dualpal.out");

        String[] line = s.nextLine().split("\\s+");
        int N = Integer.parseInt(line[0]);
        int S = Integer.parseInt(line[1]);
        int total = 0;
        for (int num = S + 1; total < N; num++)
        {
            int count = 0;
            for (int B = 2; B <= 10; B++)
            {
                if (isPali(rep(num, B)))
                    count++;
                if (count == 2)
                {
                    p.println(num);
                    total++;
                    break;
                }
            }
        }
        p.close();
    }

    private static String rep(int N, int B)
    {
        String num = "";
        int init = (int)(Math.log10(N) / Math.log10(B));
        for (int i = init; i >= 0; i--)
        {
            int pow = (int)Math.pow(B, i);
            if (pow > N)
                num += "0";
            else if (pow <= N)
            {
                int q = N / pow;
                if (q >= 10)
                    num += (char)(q + 55);
                else
                    num += q;
                N = N - q * pow;
            }
        }
        return num;
    }

    private static boolean isPali(String s)
    {
        for (int i = 0; i <= s.length() / 2; i++)
        {
            if (s.charAt(i) != s.charAt(s.length() - 1 - i))
                return false;
        }
        return true;
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}