/*
ID: arjun.s4
LANG: JAVA
TASK: pprime
*/

import java.util.*;
import java.io.*;

public class pprime
{
    private static ArrayList<Integer> all = new ArrayList<Integer>();

    public static void main(String[] args)
    {
        Scanner s = IO.getScan("pprime.in");
        PrintWriter p = IO.getPW("pprime.out");

        int a = s.nextInt();
        int b = s.nextInt();

        if (a % 2 == 0)
            a++;
        if (b % 2 == 0)
            b--;
        int num = String.valueOf(b).length();
        for (int i = 1; i <= num; i += 2)
            odd(a, b, i / 2, 0, "", "");
        for (int i = 2; i <= num; i += 2)
            even(a, b, i / 2, 0, "", "");
        Collections.sort(all);
        for (Integer i : all)
            p.println(i);
        p.close();
    }

    public static void odd(int a, int b, int num, int curr, String top, String bot)
    {
        if (curr > num)
        {
            int res = Integer.parseInt(top + bot);
            if (res >= a && res <= b && isPrime(res))
                all.add(res);
        }
        else if (curr == 0)
        {
            if (num == 0)
            {
                for (int c = 1; c <= 9; c += 2)
                    odd(a, b, num, curr + 1, "", c + "");
            }
            else
            {
                for (int c = 1; c <= 9; c += 2)
                    odd(a, b, num, curr + 1, c + "", c + "");
            }
        }
        else if (curr == num)
        {
            for (int c = 0; c <= 9; c++)
                odd(a, b, num, curr + 1, top, c + bot);
        }
        else
        {
            for (int c = 0; c <= 9; c++)
                odd(a, b, num, curr + 1, top + c, c + bot);
        }
    }

    public static void even(int a, int b, int num, int curr, String top, String bot)
    {
        if (curr == num)
        {
            int res = Integer.parseInt(top + bot);
            if (res >= a && res <= b && isPrime(res))
                all.add(res);
        }
        else if (curr == 0)
        {
            for (int c = 1; c <= 9; c += 2)
                even(a, b, num, curr + 1, c + "", c + "");
        }
        else
        {
            for (int c = 0; c <= 9; c++)
                even(a, b, num, curr + 1, top + c, c + bot);
        }
    }

    public static boolean isPrime(int i)
    {
        for (int d = 3; d <= Math.sqrt(i); d += 2)
        {
            if (i % d == 0)
                return false;
        }
        return true;
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}