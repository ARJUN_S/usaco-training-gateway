/*
ID: arjun.s4
LANG: JAVA
TASK: gift1
*/

import java.util.Scanner;
import java.io.*;
import java.util.HashMap;

public class gift1
{
    public static void main(String[] args)
    {
        Scanner s = IO.getScan("gift1.in");
        PrintWriter p = IO.getPW("gift1.out");

        HashMap<String, Integer> balances = new HashMap<String, Integer>();
        String[] order = null;
        int NP = 0;
        int count = 1;
        int subcount = 1;
        String curPerson = null;
        int curIndGift = 0;
        int numRec = 0;
        while (s.hasNextLine())
        {
            String line = s.nextLine();
            System.out.println(line);
            if (count == 1)
            {
                NP = Integer.parseInt(line);
                order = new String[NP];
            }
            else if (count >= 2 && count <= NP + 1)
            {
                balances.put(line, 0);
                order[count - 2] = line;
            }
            else
            {
                if (subcount == 1)
                    curPerson = line;
                else if (subcount == 2)
                {
                    String[] arr = line.split("\\s+");
                    numRec = Integer.parseInt(arr[1]);
                    if (numRec == 0)
                    {
                        subcount = 1;
                        count++;
                        continue;
                    }
                    curIndGift = Integer.parseInt(arr[0]) / numRec;
                    int leftover = Integer.parseInt(arr[0]) - curIndGift * numRec;
                    balances.put(curPerson, balances.get(curPerson) - Integer.parseInt(arr[0]) + leftover);
                }
                else
                {
                    balances.put(line, balances.get(line) + curIndGift);
                    if (subcount == numRec + 2)
                        subcount = 0;
                }
                subcount++;
            }
            count++;
        }
        for (String name : order)
            p.println(name + " " + balances.get(name));
        p.close();
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}