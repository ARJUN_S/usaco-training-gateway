/*
ID: arjun.s4
LANG: JAVA
TASK: sort3
*/

import java.util.*;
import java.util.concurrent.*;
import java.io.*;

public class sort3
{
    public static void main(String[] args)
    {
        Scanner s = IO.getScan("sort3.in");
        PrintWriter pw = IO.getPW("sort3.out");

        int N = s.nextInt();
        int[] seq = new int[N];
        int idx = 0;
        while (idx < N)
        {
            seq[idx] = s.nextInt();
            idx++;
        }

        int[] counts = {0, 0, 0};
        for (int i = 0; i < N; i++)
            counts[seq[i] - 1]++;
        int numEx = 0;
        for (int i = 0; i < N; i++)
        {
            int j = -1;
            if (seq[i] == 1)
            {
                if (i >= counts[0] && i <= counts[0] + counts[1] - 1)
                    j = search(seq, 0, counts[0] - 1, 2, counts[0] + counts[1], N - 1);
                else if (i >= counts[0] + counts[1])
                    j = search(seq, 0, counts[0] - 1, 3, counts[0], counts[0] + counts[1] - 1);
            }
            else if (seq[i] == 2)
            {
                if (i <= counts[0] - 1)
                    j = search(seq, counts[0], counts[0] + counts[1] - 1, 1, counts[0] + counts[1], N - 1);
                else if (i >= counts[0] + counts[1])
                    j = search(seq, counts[0], counts[0] + counts[1] - 1, 3, 0, counts[0] - 1);
            }
            else
            {
                if (i <= counts[0] - 1)
                    j = search(seq, counts[0] + counts[1], N - 1, 1, counts[0], counts[0] + counts[1] - 1);
                else if (i >= counts[0] && i <= counts[0] + counts[1] - 1)
                    j = search(seq, counts[0] + counts[1], N - 1, 2, 0, counts[0] - 1);
            }
            if (j != -1)
            {
                int temp = seq[i];
                seq[i] = seq[j];
                seq[j] = temp;
                numEx++;
            }
        }
        System.out.println(Arrays.toString(seq));
        pw.println(numEx);
        pw.close();
    }

    public static int search(int[] seq, int i, int j, int target, int m, int n)
    {
        for (int k = i; k <= j; k++)
        {
            if (seq[k] == target)
                return k;
        }
        for (int k = m; k <= n; k++)
        {
            if (seq[k] == target)
                return k;
        }
        return -1;
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}