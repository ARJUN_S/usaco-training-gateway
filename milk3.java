/*
ID: arjun.s4
LANG: JAVA
TASK: milk3
*/

import java.util.*;
import java.io.*;

@SuppressWarnings("unchecked")
public class milk3
{
    private static int total;

    public static void main(String[] args)
    {
        Scanner s = IO.getScan("milk3.in");
        PrintWriter p = IO.getPW("milk3.out");

        int A = s.nextInt();
        int B = s.nextInt();
        int C = s.nextInt();
        ArrayList<Integer> list = new ArrayList<Integer>();
        HashSet<Triple<Integer, Integer, Integer>> vis = new HashSet<Triple<Integer, Integer, Integer>>();
        total = C;
        ArrayList<Integer> res = getAll(A, B, C, 0, 0, C, list, vis);
        Collections.sort(res);
        int count = 0;
        for (Integer i : res)
        {
            if (count == 0)
                p.print(i);
            else
                p.print(" " + i);
            count++;
        }
        p.println();
        p.close();
    }

    public static ArrayList<Integer> getAll(int A, int B, int C, int a, int b, int c, ArrayList<Integer> list, HashSet<Triple<Integer, Integer, Integer>> vis)
    {
        if (a > A || b > B || c > C || a + b + c != total || vis.contains(new Triple(a, b, c)) || (a == 0 && list.contains(c)))
            return null;
        else
            vis.add(new Triple(a, b, c));
        if (a == 0)
            list.add(c);

        if (b + a > B)
            getAll(A, B, C, a - B + b, B, c, list, vis);
        else
            getAll(A, B, C, 0, b + a, c, list, vis);
        if (a + b > A)
            getAll(A, B, C, A, a + b - A, c, list, vis);
        else
            getAll(A, B, C, a + b, 0, c, list, vis);

        if (c + a > C)
            getAll(A, B, C, a - C + c, b, C, list, vis);
        else
            getAll(A, B, C, 0, b, c + a, list, vis);
        if (a + c > A)
            getAll(A, B, C, A, a + c - A, 0, list, vis);
        else
            getAll(A, B, C, a + c, b, 0, list, vis);
        
        if (c + b > C)
            getAll(A, B, C, a, b - C + c, C, list, vis);
        else
            getAll(A, B, C, a, 0, c + b, list, vis);
        if (b + c > B)
            getAll(A, B, C, a, B, b + c - B, list, vis);
        else
            getAll(A, B, C, a, b + c, 0, list, vis);

        return list;
    }
}

class Triple<F, S, T> {

    public final F first;
    public final S second;
    public final T third;

    public Triple(F first, S second, T third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Triple)) {
            return false;
        }
        Triple<?, ?, ?> p = (Triple<?, ?, ?>) o;
        return first.equals(p.first) && second.equals(p.second) && third.equals(p.third);
    }

    private static boolean equals(Object x, Object y) {
        return (x == null && y == null) || (x != null && x.equals(y));
    }

    @Override
    public int hashCode() {
        return (first == null ? 0 : first.hashCode()) ^ (second == null ? 0 : second.hashCode()) ^ (third == null ? 0 : third.hashCode());
    }

    public static <F, S, T> Triple <F, S, T> create(F f, S s, T t) {
        return new Triple<F, S, T>(f, s, t);
    }

    public String toString()
    {
        return "(" + first + ", " + second + ", " + third + ")";
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}