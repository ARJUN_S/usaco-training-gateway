/*
ID: arjun.s4
LANG: JAVA
TASK: transform
*/

import java.util.*;
import java.io.*;

public class transform
{
    public static void main(String[] args)
    {
        Scanner s = IO.getScan("transform.in");
        PrintWriter p = IO.getPW("transform.out");

        int N = Integer.parseInt(s.nextLine().trim());
        char[][] pTiles = new char[N][N];
        char[][] aTiles = new char[N][N];
        fill(pTiles, N, s);
        fill(aTiles, N, s);

        if (equal(rotate90(pTiles), aTiles))
            p.println(1);
        else if (equal(rotate90(rotate90(pTiles)), aTiles))
            p.println(2);
        else if (equal(rotate90(rotate90(rotate90(pTiles))), aTiles))
            p.println(3);
        else if (equal(reflect(pTiles), aTiles))
            p.println(4);
        else if (equal(pTiles, aTiles))
            p.println(6);
        else
        {
            pTiles = reflect(pTiles);
            if (equal(rotate90(pTiles), aTiles))
                p.println(5);
            else if (equal(rotate90(rotate90(pTiles)), aTiles))
                p.println(5);
            else if (equal(rotate90(rotate90(rotate90(pTiles))), aTiles))
                p.println(5);
            else
                p.println(7);
        }
        p.close();
    }

    private static void fill(char[][] arr, int N, Scanner s)
    {
        for (int i = 0; i < N; i++)
        {
            String line = s.nextLine().trim();
            for (int c = 0; c < N; c++)
                arr[i][c] = line.charAt(c);
        }
    }

    private static char[][] rotate90(char[][] arr)
    {
        char[][] newArr = new char[arr.length][arr.length];
        for (int c = 0; c < arr.length; c++)
        {
            for (int r = 0; r < arr.length; r++)
                newArr[c][arr.length - 1 - r] = arr[r][c];
        }
        return newArr;
    }

    private static char[][] reflect(char[][] arr)
    {
        char[][] newArr = new char[arr.length][arr.length];
        for (int r = 0; r < arr.length; r++)
        {
            for (int c = 0; c < arr[0].length; c++)
                newArr[r][arr.length - 1 - c] = arr[r][c];
        }
        return newArr;
    }

    private static boolean equal(char[][] arr1, char[][] arr2)
    {
        for (int r = 0; r < arr1.length; r++)
        {
            for (int c = 0; c < arr1[0].length; c++)
            {
                if (arr1[r][c] != arr2[r][c])
                    return false;
            }
        }
        return true;
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}