/*
ID: arjun.s4
LANG: JAVA
TASK: milk2
*/

import java.util.*;
import java.io.*;

public class milk2
{
    public static void main(String[] args)
    {
        Scanner s = IO.getScan("milk2.in");
        PrintWriter p = IO.getPW("milk2.out");

        int N = Integer.parseInt(s.nextLine().trim());
        int[] starts = new int[N];
        int[] ends = new int[N];
        int i = 0;
        while (s.hasNextLine())
        {
            String[] parts = s.nextLine().trim().split("\\s+");
            starts[i] = Integer.parseInt(parts[0]);
            ends[i] = Integer.parseInt(parts[1]);
            i++;
        }
        bubbleSort(starts);
        bubbleSort(ends);

        int prevStart = starts[0];
        int prevEnd = ends[0];
        int longestM = prevEnd - prevStart;
        int longestI = 0;
        for (i = 1; i < N; i++)
        {
            int curStart = starts[i];
            int curEnd = ends[i];
            if (curStart <= prevEnd)
            {
                if (curEnd - prevStart > longestM)
                    longestM = curEnd - prevStart;
                prevEnd = curEnd;
            }
            else
            {
                if (curStart - prevEnd > longestI)
                    longestI = curStart - prevEnd;
                prevStart = curStart;
                prevEnd = curEnd;
            }
        }
        p.println(longestM + " " + longestI);
        p.close();
    }

    private static void bubbleSort(int[] array)
    {
        int n = array.length;
        int k;
        for (int m = n; m >= 0; m--)
        {
            for (int i = 0; i < n - 1; i++)
            {
                k = i + 1;
                if (array[i] > array[k])
                    swapNumbers(i, k, array);
            }
        }
    }
  
    private static void swapNumbers(int i, int j, int[] array)
    {
        int temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}