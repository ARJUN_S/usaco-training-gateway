/*
ID: arjun.s4
LANG: JAVA
TASK: friday
*/

import java.util.*;
import java.io.*;

public class friday
{
    public static void main(String[] args)
    {
        Scanner s = IO.getScan("friday.in");
        PrintWriter p = IO.getPW("friday.out");

        int N = Integer.parseInt(s.nextLine());

        HashMap<String, Integer> days = new HashMap<String, Integer>();
        List<String> putIn = Arrays.asList("Saturday", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday");
        for (String weekday : putIn)
            days.put(weekday, 0);

        String weekday = "Monday";
        int day = 1;
        int month = 1;
        int year = 1900;

        List<Integer> thirty = Arrays.asList(9, 4, 6, 11);
        List<Integer> thirtyone = Arrays.asList(1, 3, 5, 7, 8, 10, 12);

        do
        {
            if (day == 13)
                days.put(weekday, days.get(weekday) + 1);

            int index = putIn.indexOf(weekday) + 1;
            if (index == putIn.size())
                weekday = putIn.get(0);
            else
                weekday = putIn.get(index);

            if ((thirty.contains(month) && day == 30) || (thirtyone.contains(month) && day == 31) || (isFebAndLeapYear(month, year) == 2 && day == 29) || (isFebAndLeapYear(month, year) == 1 && day == 28))
            {
                if (month == 12)
                {
                    year++;
                    month = 1;
                }
                else
                    month++;
                day = 1;
            }
            else
                day++;
        } while (day != 1 || month != 1 || year != 1900 + N);

        String output = "";
        for (String elem : putIn)
        {
            output += days.get(elem) + " ";
        }
        p.println(output.trim());
        p.close();
    }

    public static int isFebAndLeapYear(int month, int year)
    {
        if (month == 2)
        {
            if (year % 100 == 0 && year % 400 != 0)
                return 1;
            if (year % 4 == 0)
                return 2;
            return 1;
        }
        return 0;
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}