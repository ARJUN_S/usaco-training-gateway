/*
ID: arjun.s4
LANG: JAVA
TASK: crypt1
*/

import java.util.*;
import java.io.*;

public class crypt1
{
    public static void main(String[] args)
    {
        Scanner s = IO.getScan("crypt1.in");
        PrintWriter p = IO.getPW("crypt1.out");

        s.nextLine();
        String[] nums = s.nextLine().split("\\s+");
        Set<Integer> top = reorder(nums, 3);
        Set<Integer> bottom = reorder(nums, 2); 

        int total = 0;
        for (int t : top)
        {
            for (int b : bottom)
            {
                int p1 = t * (b % 10);
                int p2 = t * (b / 10);
                if (valid(p1, nums, 3) && valid(p2, nums, 3) && valid(p1 + (p2 * 10), nums, 4))
                {
                    total++;
                }
            }
        }
        p.println(total);
        p.close();
    }

    public static Set<Integer> reorder(String[] nums, int type)
    {
        Set<Integer> set = new HashSet<Integer>();
        for (int n = 0; n < nums.length; n++)
        {
            for (int n2 = 0; n2 < nums.length; n2++)
            {
                if (type == 3)
                {
                    for (int n3 = 0; n3 < nums.length; n3++)
                        set.add(Integer.parseInt(nums[n] + nums[n2] + nums[n3]));
                }
                else
                    set.add(Integer.parseInt(nums[n] + nums[n2]));
            }
        }
        return set;
    }

    public static boolean valid(int p, String[] nums, int maxl)
    {
        String s = String.valueOf(p);
        if (s.length() > maxl)
            return false;
        for (int i = 0; i < s.length(); i++)
        {
            if (!Arrays.asList(nums).contains(s.substring(i, i + 1)))
                return false;
        }
        return true;
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}