/*
ID: arjun.s4
LANG: JAVA
TASK: ride
*/

import java.util.Scanner;
import java.io.*;

public class ride
{
    public static void main(String[] args)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File("ride.in"));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new FileWriter(new File("ride.out"), true));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        while (s.hasNextLine())
        {
            String comet = s.nextLine();
            String group = s.nextLine();
            if (getProd(comet) % 47 == getProd(group) % 47)
                p.println("GO");
            else
                p.println("STAY");
        }
        p.close();
    }

    public static long getProd(String line)
    {
        long product = 1;
        for (int i = 0; i < line.length(); i++)
        {
            product *= (line.charAt(i) - 64);
        }
        System.out.println(product);
        return product;
    }
}