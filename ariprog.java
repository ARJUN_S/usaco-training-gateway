/*
ID: arjun.s4
LANG: JAVA
TASK: ariprog
*/

import java.util.*;
import java.io.*;

public class ariprog
{
    public static void main(String[] args)
    {
        Scanner s = IO.getScan("ariprog.in");
        PrintWriter pw = IO.getPW("ariprog.out");

        int N = s.nextInt();
        int M = s.nextInt();

        boolean[] bisquares = new boolean[2 * M * M + 1];
        for (int p = 0; p <= M; p++)
        {
            for (int q = p; q <= M; q++)
                bisquares[p * p + q * q] = true;
        }
       
        ArrayList<Pair> disc = new ArrayList<Pair>();
        for (int ref = 0; ref < bisquares.length - 1; ref++)
        {
            if (bisquares[ref] == false)
                continue;
            outer:
            for (int diff = 1; diff <= (bisquares.length - 1 - ref) / (N - 1); diff++)
            {
                int count = 1;
                while (count < N)
                {
                    if (bisquares[ref + count * diff] == false)
                        continue outer;
                    count++;
                }
                disc.add(new Pair(ref, diff));
            }
        }
        if (disc.size() == 0)
            pw.println("NONE");
        else
        {
            Collections.sort(disc, new Comparator<Pair>()
            {
                public int compare(Pair p1, Pair p2)
                {
                    return p1.getTwo() - p2.getTwo();
                }
            });
            for (Pair p : disc)
                pw.println(p.getOne() + " " + p.getTwo());
        }
        pw.close();
    }
}

class Pair
{
    private Integer one;
    private Integer two;

    public Pair(Integer one, Integer two)
    {
        this.one = one;
        this.two = two;
    }

    public Integer getOne()
    {
        return one;
    }

    public Integer getTwo()
    {
        return two;
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}