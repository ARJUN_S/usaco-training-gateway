/*
ID: arjun.s4
LANG: JAVA
TASK: holstein
*/

import java.util.*;
import java.io.*;

public class holstein
{
    public static void main(String[] args)
    {
        Scanner s = IO.getScan("holstein.in");
        PrintWriter p = IO.getPW("holstein.out");

        int V = s.nextInt();
        int[] minReq = new int[V];
        for (int v = 0; v < V; v++)
            minReq[v] = s.nextInt();
        int G = s.nextInt();
        HashMap<int[], ArrayList<int[]>> graph = new HashMap<int[], ArrayList<int[]>>();
        for (int g = 1; g <= G; g++)
        {
            int[] feed = new int[V + 1];
            feed[0] = g;
            for (int v = 1; v < V + 1; v++)
                feed[v] = s.nextInt();
            graph.put(feed, null);
        }
        for (int[] feed : graph.keySet())
        {
            ArrayList<int[]> others = new ArrayList<int[]>();
            for (int[] f : graph.keySet())
            {
                if (Arrays.equals(feed, f))
                    continue;
                others.add(f);
            }
            graph.put(feed, others);
        }

        ArrayList<int[]> res = null;
        int count = 0;
        for (int[] startVal : graph.keySet())
        {
            ArrayList<int[]> ans = BFS(graph, startVal, minReq);
            if (count == 0)
                res = ans;
            else if (ans.size() <= res.size())
            {
                if (ans.size() == res.size())
                {
                    if (feedSum(ans) < feedSum(res))
                        res = ans;
                }
                else
                    res = ans;
            }
            count++;
        }
        ArrayList<Integer> toSort = new ArrayList<Integer>();
        for (int[] feed : res)
            toSort.add(feed[0]);
        Collections.sort(toSort);
        String out = "";
        out += res.size() + " ";
        for (Integer feed : toSort)
            out += feed + " ";
        p.println(out.substring(0, out.length() - 1));
        p.close();
    }

    public static int feedSum(ArrayList<int[]> res)
    {
        int sum = 0;
        for (int[] feed : res)
        {
            for (int v = 1; v < feed.length; v++)
                sum += feed[v];
        }
        return sum;
    }

    public static ArrayList<int[]> BFS(HashMap<int[], ArrayList<int[]>> g, int[] s, int[] standard)
    {
        ArrayList<ArrayList<int[]>> queue = new ArrayList<ArrayList<int[]>>();
        ArrayList<int[]> path = new ArrayList<int[]>();
        path.add(s);
        queue.add(path);
        int count = 0;
        while (queue.size() != 0)
        {
            path = queue.remove(0);
            int[] v = path.get(path.size() - 1);
            if (stop(standard, path))
                return path;
            for (int[] w : g.get(v))
            {
                if (!path.contains(w))
                {
                    ArrayList<int[]> newPath = new ArrayList<int[]>(path);
                    newPath.add(w);
                    queue.add(newPath);
                }
            }
            count++;
        }
        return null;
    }

    public static boolean stop(int[] standard, ArrayList<int[]> feeds)
    {
        int[] total = new int[standard.length];
        for (int[] feed : feeds)
        {
            for (int i = 0; i < total.length; i++)
                total[i] += feed[i + 1];
        }
        for (int i = 0; i < total.length; i++)
        {
            if (total[i] < standard[i])
                return false;
        } 
        return true;
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}