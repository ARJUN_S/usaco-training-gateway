/*
ID: arjun.s4
LANG: JAVA
TASK: namenum
*/

import java.util.*;
import java.io.*;

public class namenum
{
    public static void main(String[] args)
    {
        Scanner s = IO.getScan("dict.txt");
        ArrayList<String> dict = new ArrayList<String>();
        while (s.hasNextLine())
            dict.add(s.nextLine().trim());

        s = IO.getScan("namenum.in");
        PrintWriter p = IO.getPW("namenum.out");

        String serial = s.nextLine().trim();
        ArrayList<String> names = new ArrayList<String>();
        Comparator<String> comp = new Comparator<String>()
        {
            public int compare(String s1, String s2)
            {
                if (s1.length() < s2.length())
                    return s1.compareTo(s2);
                return s1.substring(0, s2.length()).compareTo(s2);
            }
        };
        for (int c = 0; c < serial.length(); c++)
        {
            char cur = serial.charAt(c);
            char[] poss = getLetters(cur);
            ArrayList<String> newNames = new ArrayList<String>();
            if (c != 0)
            {
                for (String name : names)
                {
                    for (char add : poss)
                    {
                        if (Collections.binarySearch(dict, name + add, comp) >= 0)
                            newNames.add(name + add);
                    }
                }
                names = newNames;
            }
            else
            {
                for (char add : poss)
                    names.add(add + "");
            }
        }
        boolean used = false;
        if (names.size() != 0)
        {
            for (String name : names)
            {
                if (Collections.binarySearch(dict, name) >= 0)
                {
                    p.println(name);
                    used = true;
                }
            }
        }
        if (!used)
            p.println("NONE");
        p.close();
    }

    private static char[] getLetters(char cur)
    {
        if (cur == '2')
            return new char[]{'A', 'B', 'C'};
        else if (cur == '3')
            return new char[]{'D', 'E', 'F'};
        else if (cur == '4')
            return new char[]{'G', 'H', 'I'};
        else if (cur == '5')
            return new char[]{'J', 'K', 'L'};
        else if (cur == '6')
            return new char[]{'M', 'N', 'O'};
        else if (cur == '7')
            return new char[]{'P', 'R', 'S'};
        else if (cur == '8')
            return new char[]{'T', 'U', 'V'};
        else if (cur == '9')
            return new char[]{'W', 'X', 'Y'};
        return new char[]{};
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}