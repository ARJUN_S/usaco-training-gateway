/*
ID: arjun.s4
LANG: JAVA
TASK: frac1
*/

import java.util.*;
import java.io.*;

public class frac1
{
    public static void main(String[] args)
    {
        Scanner s = IO.getScan("frac1.in");
        PrintWriter pw = IO.getPW("frac1.out");

        int N = s.nextInt();

        HashMap<Double, String> out = new HashMap<Double, String>();
        for (int d = 1; d <= N; d++)
        {
            for (int n = 0; n <= d; n++)
            {
                double res = (double)n / d;
                if (!out.containsKey(res))
                    out.put(res, n + "/" + d);
                else
                {
                    int denom = Integer.parseInt(out.get(res).substring(out.get(res).length() - 1, out.get(res).length()));
                    if (d < denom)
                        out.put(res, n + "/" + d);
                }
            }
        }
        ArrayList<Double> order = new ArrayList<Double>();
        order.addAll(out.keySet());
        Collections.sort(order);
        for (Double val : order)
            pw.println(out.get(val));
        pw.close();
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}