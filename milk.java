/*
ID: arjun.s4
LANG: JAVA
TASK: milk
*/

import java.util.*;
import java.io.*;

public class milk
{
    public static void main(String[] args)
    {
        Scanner s = IO.getScan("milk.in");
        PrintWriter p = IO.getPW("milk.out");

        String[] line = s.nextLine().split("\\s+");
        int total = Integer.parseInt(line[0]);
        int numFarmers = Integer.parseInt(line[1]);

        Pair[] PUPairs = new Pair[numFarmers];
        for (int i = 0; i < numFarmers; i++)
            PUPairs[i] = new Pair(s.nextLine());

        Arrays.sort(PUPairs, new Comparator<Pair>()
        {
            public int compare(Pair p1, Pair p2)
            {
                return p1.getOne().compareTo(p2.getOne());
            }
        });

        int cPrice = 0;
        int numUnits = 0;
        for (int i = 0; i < numFarmers; i++)
        {
            if (numUnits + PUPairs[i].getTwo() > total)
            {
                cPrice += (total - numUnits) * PUPairs[i].getOne();
                break;
            }
            else
                cPrice += PUPairs[i].getTwo() * PUPairs[i].getOne();
            numUnits += PUPairs[i].getTwo();
        }
        p.println(cPrice);
        p.close();
    }
}

class Pair
{
    private Integer one;
    private Integer two;

    public Pair(String line)
    {
        String[] parts = line.split("\\s+");
        one = Integer.parseInt(parts[0]);
        two = Integer.parseInt(parts[1]);
    }

    public Integer getOne()
    {
        return one;
    }

    public Integer getTwo()
    {
        return two;
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}