/*
ID: arjun.s4
LANG: JAVA
TASK: combo
*/

import java.util.*;
import java.io.*;

public class combo
{
    public static void main(String[] args)
    {
        Scanner s = IO.getScan("combo.in");
        PrintWriter p = IO.getPW("combo.out");

        int N = s.nextInt();
        Integer[] comb1 = {s.nextInt(), s.nextInt(), s.nextInt()};
        Integer[] comb2 = {s.nextInt(), s.nextInt(), s.nextInt()};

        Set<String> r = count(comb1, N);
        r.addAll(count(comb2, N));

        p.println(r.size());
        p.close();
    }

    public static Set<String> count(Integer[] comb, int N)
    {
        Set<String> r = new HashSet<String>();
        for (int n = comb[0] - 2; n <= comb[0] + 2; n++)
        {
            int nc = corr(n, N);
            for (int n2 = comb[1] - 2; n2 <= comb[1] + 2; n2++)
            {
                int n2c = corr(n2, N);
                for (int n3 = comb[2] - 2; n3 <= comb[2] + 2; n3++)
                {
                    int n3c = corr(n3, N);
                    r.add(nc + "," + n2c + "," + n3c);
                }
            }
        }
        return r;
    }

    public static int corr(int num, int N)
    {
        int r = num % N;
        if (r < 0)
            r += N;
        return r;
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}