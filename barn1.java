/*
ID: arjun.s4
LANG: JAVA
TASK: barn1
*/

import java.util.*;
import java.io.*;

public class barn1
{
    public static void main(String[] args)
    {
        Scanner s = IO.getScan("barn1.in");
        PrintWriter p = IO.getPW("barn1.out");

        String[] parts = s.nextLine().trim().split("\\s+");
        int maxBoards = Integer.parseInt(parts[0]);
        
        ArrayList<Integer> stallNums = new ArrayList<Integer>();
        while (s.hasNextLine())
            stallNums.add(Integer.parseInt(s.nextLine().trim()));
        Collections.sort(stallNums);
        stallNums.add(0);

        boolean inSeq = false;
        int lastEnd = 0;
        ArrayList<Integer> dist = new ArrayList<Integer>();
        for (int i = 0; i < stallNums.size() - 1; i++)
        {
            if (stallNums.get(i) + 1 == stallNums.get(i + 1))
            {
                if (!inSeq)
                {
                    if (i != 0)
                        dist.add(stallNums.get(i) - lastEnd);
                    inSeq = true;
                }
            }
            else if (inSeq)
            {
                lastEnd = stallNums.get(i);
                inSeq = false;
            }
            else
            {
                if (i != 0)
                    dist.add(stallNums.get(i) - lastEnd);
                lastEnd = stallNums.get(i);
            }
        }
        stallNums.remove(stallNums.size() - 1);

        Collections.sort(dist, new Comparator<Integer>()
        {
            public int compare(Integer i1, Integer i2)
            {
                return i1.compareTo(i2);
            }
        });
        System.out.println(dist);

        int numStalls = stallNums.size();
        int i = 0;
        for (int numBoards = dist.size(); numBoards >= maxBoards; numBoards--)
        {
            numStalls += dist.get(i) - 1;
            i++;
        }
        p.println(numStalls);
        p.close();
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}