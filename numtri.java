/*
ID: arjun.s4
LANG: JAVA
TASK: numtri
*/

import java.util.*;
import java.io.*;

public class numtri
{
    public static void main(String[] args)
    {
        Scanner s = IO.getScan("numtri.in");
        PrintWriter p = IO.getPW("numtri.out");

        int R = s.nextInt();
        int[][] tri = new int[R][R];
        for (int r = 0; r < R; r++)
        {
            for (int c = 0; c <= r; c++)
                tri[r][c] = s.nextInt();
        }
        int sum = calcLargest(tri, R - 1);
        p.println(sum);
        p.close();
    }

    public static int calcLargest(int[][] tri, int R)
    {
        for (int r = R - 1; r >= 0; r--)
        {
            for (int c = 0; c <= r; c++)
                tri[r][c] += Math.max(tri[r + 1][c], tri[r + 1][c + 1]);
        }
        return tri[0][0];
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}