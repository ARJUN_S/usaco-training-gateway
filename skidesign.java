/*
ID: arjun.s4
LANG: JAVA
TASK: skidesign
*/

import java.util.*;
import java.io.*;

public class skidesign
{
    public static void main(String[] args)
    {
        Scanner s = IO.getScan("skidesign.in");
        PrintWriter p = IO.getPW("skidesign.out");

        int N = s.nextInt();
        ArrayList<Integer> heights = new ArrayList<Integer>();
        while (s.hasNextInt())
            heights.add(s.nextInt());

        ArrayList<Integer> copy = new ArrayList<Integer>(heights);

        Collections.sort(heights);
        int min = heights.get(0);
        int max = heights.get(N - 1);
        while (max - min > 17)
        {
            int x = 0;
            while ((max - x) - (min + x) > 17)
                x++;
            heights.remove(N - 1);
            heights.remove(0);
            insert(min + x, heights);
            insert(max - x, heights);
            min = heights.get(0);
            max = heights.get(N - 1);
        }
        int mintotal = 0;
        for (int m = Math.max(min - 1, 0) - min; m <= Math.min(max + 1, 100) - max; m++)
        {
            int total = 0;
            for (int i = 0; i < copy.size(); i++)
            {
                if (copy.get(i) < min + m)
                    total += (int)Math.pow(min + m - copy.get(i), 2);
                else if (copy.get(i) > max + m)
                    total += (int)Math.pow(copy.get(i) - (max + m), 2);
            }
            if (m == Math.max(min - 1, 0) - min)
                mintotal = total;
            else
                mintotal = Math.min(mintotal, total);
        }
        p.println(mintotal);
        p.close();
    }

    public static void insert(int x, ArrayList<Integer> heights)
    {
        int pos = Collections.binarySearch(heights, x);
        if (pos < 0)
            heights.add(-pos - 1, x);
        else
            heights.add(pos, x);
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}