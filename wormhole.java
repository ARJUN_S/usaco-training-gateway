/*
ID: arjun.s4
LANG: JAVA
TASK: wormhole
*/

import java.util.*;
import java.io.*;

public class wormhole
{
    public static void main(String[] args)
    {
        Scanner s = IO.getScan("wormhole.in");
        PrintWriter p = IO.getPW("wormhole.out");

        ArrayList<Pair<Double>> coor = new ArrayList<Pair<Double>>();
        int N = s.nextInt();
        while (s.hasNextDouble())
            coor.add(new Pair<Double>(s.nextDouble(), s.nextDouble()));

        Collections.sort(coor, new Comparator<Pair<Double>>()
        {
            public int compare(Pair<Double> p1, Pair<Double> p2)
            {
                if (p1.getOne() == p2.getOne())
                    return (int)(p1.getTwo() - p2.getTwo());
                else
                    return (int)(p1.getOne() - p2.getOne());
            }
        });

        ArrayList<Pair<Pair<Double>>> coorpairs = new ArrayList<Pair<Pair<Double>>>();
        for (int i = 0; i < coor.size() - 1; i++)
        {
            for (int j = i + 1; j < coor.size(); j++)
                coorpairs.add(new Pair<Pair<Double>>(coor.get(i), coor.get(j)));
        }
        int total = 0;
        outer:
        for (int i = 0; i < N - 1; i++)
        {
            ArrayList<Pair<Double>> flags = new ArrayList<Pair<Double>>();
            flags.add(coorpairs.get(i).getOne());
            flags.add(coorpairs.get(i).getTwo());
            ArrayList<Pair<Pair<Double>>> subset = new ArrayList<Pair<Pair<Double>>>();
            subset.add(coorpairs.get(i));
            for (int j = N - 1; j < coorpairs.size(); j++)
            {
                if (flags.contains(coorpairs.get(j).getOne()) || flags.contains(coorpairs.get(j).getTwo()))
                    continue;
                flags.add(coorpairs.get(j).getOne());
                flags.add(coorpairs.get(j).getTwo());
                subset.add(coorpairs.get(j));
            }
            if (test(subset))
                total++;
        }
        p.println(total);
        p.close();
    }

    public static boolean test(ArrayList<Pair<Pair<Double>>> sub)
    {
        System.out.println("\n\n" + sub);
        Pair<Double> xr = xRange(sub);
        Pair<Double> beg = sub.get(0).getOne();
        Pair<Double> cur = sub.get(0).getOne();
        while (true)
        {
            cur = new Pair<Double>(cur.getOne() + 0.5, cur.getTwo());
            if (equals(beg, cur))
                return true;
            Pair<Double> pa = contains(sub, cur);
            System.out.println(cur + ", " + pa);
            if (pa != null)
            {
                if (equals(pa, beg))
                    return true;
                else
                    cur = new Pair<Double>(pa.getOne() + 0.5, pa.getTwo());
            }
            else if (cur.getOne() < xr.getOne() || cur.getOne() > xr.getTwo())
            {
                return false;
            }
        }
    }

    public static Pair<Double> contains(ArrayList<Pair<Pair<Double>>> sub, Pair<Double> coor)
    {
        for (Pair<Pair<Double>> p : sub)
        {
            Pair<Double> pa = p.getOne();
            if (equals(pa, coor))
                return p.getTwo();
            else
            {
                pa = p.getTwo();
                if (equals(pa, coor))
                    return p.getOne();
            }
        }
        return null;
    }

    public static Pair<Double> xRange(ArrayList<Pair<Pair<Double>>> sub)
    {
        double largest = 0;
        double smallest = 0;
        for (Pair<Pair<Double>> p : sub)
        {
            Pair<Double> pa = p.getOne();
            if (pa.getOne() > largest)
                largest = pa.getOne();
            else if (pa.getOne() < smallest)
                smallest = pa.getOne();
            pa = p.getTwo();
            if (pa.getOne() > largest)
                largest = pa.getOne();
            else if (pa.getOne() < smallest)
                smallest = pa.getOne();
        }
        return new Pair<Double>(smallest, largest);
    }

    public static boolean equals(Pair<Double> curr, Pair<Double> other)
    {
        return curr.getOne().doubleValue() == other.getOne().doubleValue() && curr.getTwo().doubleValue() == other.getTwo().doubleValue();
    }
}

@SuppressWarnings("rawtypes")
class Pair<E extends Object>
{
    private E one;
    private E two;

    public Pair(E one, E two)
    {
        this.one = one;
        this.two = two;
    }

    public E getOne()
    {
        return one;
    }

    public E getTwo()
    {
        return two;
    }

    public String toString()
    {
        return "(" + one + ", " + two + ")";
    }
}

class IO
{
    public static Scanner getScan(String name)
    {
        Scanner s = null;
        try
        {
            s = new Scanner(new File(name));
        }
        catch (FileNotFoundException e)
        {
            System.exit(0);
        }
        return s;
    }

    public static PrintWriter getPW(String name)
    {
        PrintWriter p = null;
        try
        {
            p = new PrintWriter(new File(name));
        }
        catch (IOException e)
        {
            System.exit(1);
        }
        return p;
    }
}